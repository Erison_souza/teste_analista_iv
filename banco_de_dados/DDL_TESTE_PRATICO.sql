--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.4
-- Dumped by pg_dump version 9.4.0
-- Started on 2015-11-16 02:53:10

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 7 (class 2615 OID 90658)
-- Name: testejava; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA testejava;


ALTER SCHEMA testejava OWNER TO postgres;

--
-- TOC entry 175 (class 3079 OID 11855)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2007 (class 0 OID 0)
-- Dependencies: 175
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 173 (class 1259 OID 90656)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hibernate_sequence OWNER TO postgres;

SET search_path = testejava, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 174 (class 1259 OID 90659)
-- Name: usuario; Type: TABLE; Schema: testejava; Owner: postgres; Tablespace: 
--

CREATE TABLE usuario (
    id_usuario integer NOT NULL,
    ativo boolean,
    cpf character varying(255) NOT NULL,
    login character varying(15) NOT NULL,
    nome character varying(200) NOT NULL,
    senha character varying(255) NOT NULL
);


ALTER TABLE usuario OWNER TO postgres;

SET search_path = public, pg_catalog;

--
-- TOC entry 2008 (class 0 OID 0)
-- Dependencies: 173
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('hibernate_sequence', 1, false);


SET search_path = testejava, pg_catalog;

--
-- TOC entry 1999 (class 0 OID 90659)
-- Dependencies: 174
-- Data for Name: usuario; Type: TABLE DATA; Schema: testejava; Owner: postgres
--

COPY usuario (id_usuario, ativo, cpf, login, nome, senha) FROM stdin;
\.


--
-- TOC entry 1884 (class 2606 OID 90668)
-- Name: uk_692bsnqxa8m9fmx7m1yc6hsui; Type: CONSTRAINT; Schema: testejava; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT uk_692bsnqxa8m9fmx7m1yc6hsui UNIQUE (cpf);


--
-- TOC entry 1886 (class 2606 OID 90670)
-- Name: uk_pm3f4m4fqv89oeeeac4tbe2f4; Type: CONSTRAINT; Schema: testejava; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT uk_pm3f4m4fqv89oeeeac4tbe2f4 UNIQUE (login);


--
-- TOC entry 1888 (class 2606 OID 90666)
-- Name: usuario_pkey; Type: CONSTRAINT; Schema: testejava; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id_usuario);


--
-- TOC entry 2006 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2015-11-16 02:53:10

--
-- PostgreSQL database dump complete
--

