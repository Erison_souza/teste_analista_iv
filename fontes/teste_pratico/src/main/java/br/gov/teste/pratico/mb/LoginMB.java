package br.gov.teste.pratico.mb;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import br.gov.teste.pratico.dao.UsuarioDAO;
import br.gov.teste.pratico.entity.Usuario;
import br.gov.teste.pratico.util.SessaoUtil;

@SessionScoped
@ManagedBean
public class LoginMB {

	/**********************************
	 * URL´S DA PÁGINA
	 ********************************/

	private static String URL_LISTA_USUARIO = "/pages/usuario/listar.xhtml?faces-redirect=true";
	/******************************
	 * VARIÁVEIS
	 *****************************/
	private String login;
	private String senha;
	private Usuario usuario;

	UsuarioDAO usuarioDao;

	public LoginMB() {
		usuarioDao = new UsuarioDAO(Usuario.class);
	}

	/**
	 * Faz o login no sistema
	 * 
	 * @return url da página principal do sistema
	 */

	public String logar() {

		// veirifica se o usuário é super-usuário
		if (login.equals("admin") && senha.equals("admin")) {
			this.usuario = new Usuario();
			this.usuario.setNome("Admin");
			this.usuario.setLogin("admin");
			this.usuario.setLogado(true);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
					"Bem Vindo " + this.usuario.getNome(), "Bem Vindo " + this.usuario.getNome()));
			return URL_LISTA_USUARIO;
		} else {
			// busca o usuário com login requerido
			this.usuario = usuarioDao.fazerLogin(login, senha);
			// verifica se retornou algum resultado
			if (this.usuario != null) {
				this.usuario.setLogado(true);
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
						"Bem Vindo " + this.usuario.getNome(), "Bem Vindo " + this.usuario.getNome()));
				return URL_LISTA_USUARIO;
			} else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"Login ou Senha Incorretos.", "Login ou Senha Incorretos."));
			}
		}

		return null;
	}

	/**
	 * Esse método serve para fazer logout do sistema.
	 * 
	 * @return
	 */
	public String logout() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/login.xhtml?faces-redirect=true";
	}

	/**
	 * O nome atual do usuário
	 * 
	 * @return dados atuaizados do usuário logado
	 */
	public String nomeUsuario() {
		if (usuario != null) {
			if (usuario.getLogin().equals("admin")) {
				return usuario.getNome();
			}
			return usuarioDao.procurarPeloId(usuario.getIdUsuario()).getNome();
		}
		return null;

	}

	/*********************************
	 * GETTERS E SETTERS
	 *********************************/

	public UsuarioDAO getUsuarioDao() {
		return usuarioDao;
	}

	public void setUsuarioDao(UsuarioDAO usuarioDao) {
		this.usuarioDao = usuarioDao;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	/**
	 * Retorna o usuário logado
	 * 
	 * @return
	 */
	public Usuario getUsuarioLogado() {
		return SessaoUtil.getUsuario();
	}

}
