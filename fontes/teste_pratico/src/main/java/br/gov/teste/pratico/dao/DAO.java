package br.gov.teste.pratico.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

import br.gov.teste.pratico.conection.ConectionJPA;

public class DAO<T> {

	private final Class<T> classe;

	private EntityManager entityManager;

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	public DAO(Class<T> classe) {
		this.classe = classe;
	}

	/**
	 * Esse método serve para adicionar uma Entidade
	 * 
	 * @param t
	 * @return
	 */
	public boolean adiciona(T t) {
		EntityManager entityManager = new ConectionJPA().getEntityManager();
		try {
			
			// abre transacao
			
			entityManager.getTransaction().begin();
			// persiste o objeto
			entityManager.persist(t);
			// commita a transacao
			entityManager.getTransaction().commit();
			// fecha a entity manager
			entityManager.close();
			return true;
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
			return false;
		}
	}

	/**
	 * Esse me´todo serve para rentityManagerover uma entidade
	 * 
	 * @param t
	 * @return
	 */

	public boolean remove(T t) {
		EntityManager entityManager = new ConectionJPA().getEntityManager();

		try {
			entityManager.getTransaction().begin();

			entityManager.remove(entityManager.merge(t));

			entityManager.getTransaction().commit();
			entityManager.close();
			return true;
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
			return false;
		}
	}

	/**
	 * Esse método serve para atualizar uma Entidade
	 * 
	 * @param t
	 * @return
	 */
	public boolean atualiza(T t) {
		EntityManager entityManager = new ConectionJPA().getEntityManager();
		try {
			entityManager.getTransaction().begin();

			entityManager.merge(t);

			entityManager.getTransaction().commit();
			entityManager.close();
			return true;
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
			return false;
		}
	}

	/**
	 * Esse método serve listar todos os resustados de uma entidade.
	 * 
	 * @return
	 */
	public List<T> listaTodos() {
		EntityManager entityManager = new ConectionJPA().getEntityManager();
		CriteriaQuery<T> query = entityManager.getCriteriaBuilder().createQuery(classe);
		query.select(query.from(classe));

		List<T> lista = entityManager.createQuery(query).getResultList();

		entityManager.close();
		return lista;
	}

	/**
	 * Esse método serve para buscar instancia de uma Entidade
	 * 
	 * @param id
	 * @return
	 */
	public T buscaPorId(Integer id) {
		EntityManager entityManager = new ConectionJPA().getEntityManager();
		T instancia = entityManager.find(classe, id);
		entityManager.close();
		return instancia;
	}

	/**
	 * Esse métod serve para salvar uma entidade.
	 * 
	 * @param t
	 * @return
	 */
	public T salvar(T t, Integer id) {

		EntityManager entityManager = new ConectionJPA().getEntityManager();
		try {
			// Inicia uma transação com o banco de dados.
			entityManager.getTransaction().begin();

			// Verifica se a entidade ainda não está salva no banco de dados.
			
			if (id == null) {
				// Salva os dados da entidade.
				entityManager.persist(t);
			} else {
				// Atualiza os dados da entidade.
				t = entityManager.merge(t);
			}
			// Finaliza a transação.
			entityManager.getTransaction().commit();
			
			
		} finally {
			entityManager.close();
		}
		
		return t;

	}

}
