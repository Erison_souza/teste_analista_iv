package br.gov.teste.pratico.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * ENTIDADE PARA DEFINIR OS ATRIBUTOS DO USUÁRIO
 * 
 * @author erison
 *
 */
@Entity
@Table(name = "usuario", schema = "testejava")
public class Usuario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6215758557498777227L;

	@Id
	@GeneratedValue
	@Column(name = "id_usuario")
	private Integer idUsuario;

	// nome do usuário
	@Column(name = "nome", nullable = false, length = 200)
	private String nome;

	// senha do usuário
	@Column(name = "senha", nullable = false)
	private String senha;
	
	//cpf do usuário
	@Column(name = "cpf", unique = true , nullable = false)
	private String cpf;

	// login do usuário
	@Column(name = "login", unique = true, length = 15, nullable = false)
	private String login;

	// flag que informa se o usuário está ativo. Podendo assumir os valores:
	// true == ativo ou false == inativo.
	@Column(name = "ativo")
	private boolean ativo;

	//variável auxiliar que define se o usuário está logado ou não. 
	@Transient
	private boolean logado;

	/**
	 * Getters e Setters
	 * 
	 */

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public boolean isLogado() {
		return logado;
	}

	public void setLogado(boolean logado) {
		this.logado = logado;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	
}
