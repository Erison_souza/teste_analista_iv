package br.gov.teste.pratico.mb;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class HomeMB {
	
	private static String URL_HOME = "/pages/home.xhtml?faces-redirect=true";
	
	public String index(){
		
		return URL_HOME;
	}
	
}
