package br.gov.teste.pratico.mb;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import br.gov.teste.pratico.util.CalculadoraUtil;
import br.gov.teste.pratico.util.MensagensUtil;

@ManagedBean
@SessionScoped
public class CalculadoraMB {

	/**********************************
	 * URL´S DA PÁGINA
	 ********************************/

	private static String URL_CALCULADORA = "/pages/calculadora.xhtml?faces-redirect=true";
	/*****************************
	 * VARIÁVEIS GLOBAIS
	 ****************************/

	private UploadedFile file;
	private StreamedContent fileDownload;

	/**
	 * Esse método pega o arquivo da tela
	 * 
	 * @param event
	 */
	public void handleFileUpload(FileUploadEvent event) {
		file = event.getFile();
		// marcos
		int l, h, numLinhasS1, numLinhasS2;
		// operação
		String operacao;
		// linhas do arquivo
		List<String> linhasDoArquivo;
		// números codificados
		String[] numerosRef = new String[20];

		// ---------------------------------------------------

		// captura as linhas do arquivo
		linhasDoArquivo = getLinhasDoArquivo(file);
		// imprime as linhas do arquivo
		CalculadoraUtil.imprimeLinhas(linhasDoArquivo);

		try {
			// calcula a largura do numeral
			l = CalculadoraUtil.getLarguraDoNumeral(linhasDoArquivo);
			try {
				// calcula a altura do numeral
				h = CalculadoraUtil.getAlturaDoNumeral(linhasDoArquivo);

				// verifica se não existe alguma restrição de largura e altura
				if (!CalculadoraUtil.temRestricaoLH(l, h)) {
					try {
						// pega o número de linhas do S1
						numLinhasS1 = CalculadoraUtil.getNumLinhaS1(linhasDoArquivo, h);
						try {
							// pega o número de linhas do S2
							numLinhasS2 = CalculadoraUtil.getNumLinhaS2(linhasDoArquivo, h, numLinhasS1);
							try {
								// pega a operação
								operacao = CalculadoraUtil.getOperacao(linhasDoArquivo, h, numLinhasS1, numLinhasS2);

								if (CalculadoraUtil.eOperacaoValida(operacao)) {
									System.out.println("- Largura do numeral: " + l);
									System.out.println("- Altura do numeral: " + h);

									System.out.println("- Número de Linhas do S1: " + numLinhasS1);

									System.out.println("- Número de Linhas do S2: " + numLinhasS2);

									System.out.println("- Operação: " + operacao);

									numerosRef = CalculadoraUtil.getNumerosCodificados(linhasDoArquivo, h, l);

									// imprime todos os números
									CalculadoraUtil.imprimirNumeros(numerosRef, l);
									System.out.println("----------------------------------");
									System.out.println("|       NÚMERO DA OPERAÇÃO       |");
									System.out.println("----------------------------------");
									// linha de referencia S2
									int linhaRefS1 = (h + 1) + 1;
									String[] s1 = CalculadoraUtil.getNumerosDaOperacao(linhasDoArquivo, h, l,
											numLinhasS1, linhaRefS1);
									System.out.println("S1:");
									CalculadoraUtil.imprimirNumeros(s1, l);

									// linha de referencia S2
									int linhaRefS2 = (h + 1) + (numLinhasS1 + 1) + 1;
									String[] s2 = CalculadoraUtil.getNumerosDaOperacao(linhasDoArquivo, h, l,
											numLinhasS2, linhaRefS2);
									System.out.println("S2:");
									CalculadoraUtil.imprimirNumeros(s2, l);

									BigDecimal vlS1 = CalculadoraUtil.calcularNumeroBase20(s1, numerosRef);
									BigDecimal vlS2 = CalculadoraUtil.calcularNumeroBase20(s2, numerosRef);
									// verifica se oo números passados com
									// entrada excedem o valor máximo
									if (!CalculadoraUtil.s1ES2ExcedeValorMaximo(vlS1, vlS2)) {
										System.out.println("Valor S1: " + vlS1);
										System.out.println("Valor S2: " + vlS2);

										BigDecimal resultado = CalculadoraUtil.realizarOperacao(vlS1, vlS2, operacao);
										System.out.println("Resultado: " + resultado);

										List<Integer> resultadoBase20 = CalculadoraUtil
												.converteDecimalParaBase20(resultado);// CalculadoraUtil.resultadoBase20(resultado);

										System.out.println("Resultado na base 20: " + resultadoBase20);

										String resultadoDeCod = CalculadoraUtil.codBase20(resultadoBase20, numerosRef);
										// imprime o resultado
										CalculadoraUtil.imprimirResultadoCod(resultadoDeCod, l);

										try {
											downloadDoResultado(resultadoDeCod, l, file.getFileName());
										} catch (IOException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}

										FacesMessage message = new FacesMessage(
												"- Resultado base 10: " + resultado + "\n.   - Resultado base 20: "
														+ CalculadoraUtil.getResultadoBase20(resultadoBase20),
												event.getFile().getFileName() + " processado com sucesso.");
										FacesContext.getCurrentInstance().addMessage(null, message);
									} else {
										FacesContext.getCurrentInstance().addMessage(null,
												new FacesMessage(FacesMessage.SEVERITY_ERROR,
														MensagensUtil.getMessagem("MN016"),
														MensagensUtil.getMessagem("MN016")));
									}
								} else {
									FacesContext.getCurrentInstance().addMessage(null,
											new FacesMessage(FacesMessage.SEVERITY_ERROR,
													MensagensUtil.getMessagem("MN012"),
													MensagensUtil.getMessagem("MN012")));
								}
							} catch (NumberFormatException ex) {
								FacesContext.getCurrentInstance().addMessage(null,
										new FacesMessage(FacesMessage.SEVERITY_ERROR,
												MensagensUtil.getMessagem("MN013"),
												MensagensUtil.getMessagem("MN013")));
							}
						} catch (Exception ex) {
							FacesContext.getCurrentInstance().addMessage(null,
									new FacesMessage(FacesMessage.SEVERITY_ERROR, MensagensUtil.getMessagem("MN015"),
											MensagensUtil.getMessagem("MN015")));
						}
					} catch (NumberFormatException ex) {
						FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
								MensagensUtil.getMessagem("MN014"), MensagensUtil.getMessagem("MN014")));
					}
				} else {
					// verficar se l pertence ao intervalo 0 < l < 100. Se não
					// pertencer
					// retorna que tem restrição (true).
					if (!(0 < l && l < 100)) {
						FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
								MensagensUtil.getMessagem("MN011"), MensagensUtil.getMessagem("MN011")));
					}
					// verficar se h pertence ao intervalo 0 < h < 100. Se não
					// pertencer
					// retorna que tem restrição (true).
					if (!(0 < h && h < 100)) {
						FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
								MensagensUtil.getMessagem("MN010"), MensagensUtil.getMessagem("MN010")));
					}

				}
			} catch (NumberFormatException ex) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
						MensagensUtil.getMessagem("MN008"), MensagensUtil.getMessagem("MN008")));
			}
		} catch (NumberFormatException ex) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					MensagensUtil.getMessagem("MN009"), MensagensUtil.getMessagem("MN009")));
		} catch (Exception ex) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					MensagensUtil.getMessagem("MN015"), MensagensUtil.getMessagem("MN015")));
		}

	}

	/**
	 * Ler o arquivo e quarda as linhas num array de spring
	 * 
	 * @param event
	 * @return Array com as linhas do arquivo.
	 */
	private List<String> getLinhasDoArquivo(UploadedFile event) {
		BufferedReader br = null;
		String line;
		List<String> linhasDoArquivo = new ArrayList<>();
		try {
			InputStream is = event.getInputstream();
			br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			while ((line = br.readLine()) != null) {
				linhasDoArquivo.add(line);
			}
			return linhasDoArquivo;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	/**
	 * Down load do resultado
	 * 
	 * @param resultadoDeCod
	 * @param l
	 * @param nomeDoArquivo
	 * @throws IOException
	 */
	private void downloadDoResultado(String resultadoDeCod, int l, String nomeDoArquivo) throws IOException {

		StringBuilder linhasDoResultado = new StringBuilder();

		for (int i = 0; i < resultadoDeCod.length(); i = i + l) {
			linhasDoResultado.append(resultadoDeCod.substring(i, i + l) + "\n");
		}

		byte[] bytes = linhasDoResultado.toString().getBytes();
		InputStream stream = new ByteArrayInputStream(bytes);
		setFileDownload(new DefaultStreamedContent(stream, "application/txt", nomeDoArquivo + ".txt"));
		if (linhasDoResultado.length() > 0) {
			RequestContext.getCurrentInstance()
					.execute("document.getElementById('calculadora:downloadResultado').click();");
		}
	}

	public String index() {

		return URL_CALCULADORA;
	}

	/************************************
	 * GETTER´S E SETTER´S
	 ************************************/

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public StreamedContent getFileDownload() {
		return fileDownload;
	}

	public void setFileDownload(StreamedContent fileDownload) {
		this.fileDownload = fileDownload;
	}

}
