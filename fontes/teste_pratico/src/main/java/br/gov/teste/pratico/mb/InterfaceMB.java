package br.gov.teste.pratico.mb;

public interface InterfaceMB {
	
	public String cadastrar();
	public String listar();
	public String alterar();
	public String inativar();
	public String salvar();
	public String pesquisar();
	public String visualizar();

}
