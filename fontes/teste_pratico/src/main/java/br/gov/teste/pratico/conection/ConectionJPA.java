package br.gov.teste.pratico.conection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Classse resposável pela conexão do sistema com o banco de dados. 
 * @author erison
 *
 */
public class ConectionJPA {

	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("Teste_Pratico-Postgres");
	
	
	public EntityManager getEntityManager() {
		return emf.createEntityManager();
	}

	public void close(EntityManager em) {
		em.close();
	}
}
