package br.gov.teste.pratico.util;

import java.math.BigInteger;
import java.security.MessageDigest;


public class MD5Util {
	
	/**
	 * Criptograva palavra com MD5
	 * @param palavra
	 * @return palavra criptografada
	 * @throws Exception
	 */
	public static String criptografarMD5(String palavra) throws Exception{
		
		MessageDigest m = MessageDigest.getInstance("MD5");
		m.update(palavra.getBytes(), 0, palavra.length());
		String palavraCriptografada = new BigInteger(1, m.digest()).toString(16);
		
		return palavraCriptografada;
		
	}
	
	

}
