package br.gov.teste.pratico.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Métodos úteis para condificar e decodificar o arquivo.
 * 
 * @author erison
 *
 */
public class CalculadoraUtil {

	/**
	 * Imprime linhas do arquivo
	 * 
	 * @param linhas
	 */
	public static void imprimeLinhas(List<String> linhas) {
		System.out.println(" - Arquivo: ");
		for (String linha : linhas) {
			System.out.println(linha);
		}
	}

	/**
	 * Calcula a largura do numeral
	 * 
	 * @param linhasDoArquivo
	 * @return largura do numeral
	 */
	public static int getLarguraDoNumeral(List<String> linhasDoArquivo) {

		int l;
		// pega a primeira posição da linha que foi seperada por " ".
		l = Integer.parseInt(linhasDoArquivo.get(0).split(" ")[0]);
		return l;
	}

	/**
	 * Calcula a altura do numeral
	 * 
	 * @param linhasDoArquivo
	 * @return altura do numeral
	 */
	public static int getAlturaDoNumeral(List<String> linhasDoArquivo) {

		int h;
		// pega a segunda posição da linha que foi seperada por " ".
		h = Integer.parseInt(linhasDoArquivo.get(0).split(" ")[1]);
		return h;
	}

	/**
	 * Pega o número de linhas do S1
	 * 
	 * @param linhasDoArquivo
	 * @param h
	 * @return número de linhas do S1
	 */
	public static int getNumLinhaS1(List<String> linhasDoArquivo, int h) {

		int numLinhasS1;

		numLinhasS1 = Integer.parseInt(linhasDoArquivo.get(h + 1).trim());
		return numLinhasS1;
	}

	/**
	 * Pega o número de linhas do S2
	 * 
	 * @param linhasDoArquivo
	 * @param h
	 * @param numLinhasS1
	 * @return número de linhas do S2
	 */
	public static int getNumLinhaS2(List<String> linhasDoArquivo, int h, int numLinhasS1) {

		int numLinhasS2;

		System.out.println((h + 1) + (numLinhasS1 + 1));
		numLinhasS2 = Integer.parseInt(linhasDoArquivo.get((h + 1) + (numLinhasS1 + 1)).trim());
		return numLinhasS2;
	}

	/**
	 * Pega a operação
	 * 
	 * @param linhasDoArquivo
	 * @param h
	 * @param numLinhasS1
	 * @param numLinhasS2
	 * @return operação
	 */
	public static String getOperacao(List<String> linhasDoArquivo, int h, int numLinhasS1, int numLinhasS2) {

		String operacao;

		operacao = linhasDoArquivo.get((h + 1) + (numLinhasS1 + 1) + (numLinhasS2 + 1)).charAt(0) + "";
		return operacao;
	}

	/**
	 * Separa os 20 números. Cada elemento da String é um número
	 * 
	 * @param linhasDoArquivo
	 * @param h
	 * @param l
	 * @return números codificados
	 */
	public static String[] getNumerosCodificados(List<String> linhasDoArquivo, int h, int l) {
		String[] numeros = new String[20];
		// zera os números
		for (int i = 0; i < numeros.length; i++) {
			numeros[i] = "";
		}
		// ler cada linha
		for (int i = 1; i < (h + 1); i++) {
			// ler a linha com largura l
			for (int j = 0, index = 0; j < (20 * l); j = j + l, index++) {
				numeros[index] = numeros[index] + linhasDoArquivo.get(i).substring(j, j + l);
			}
		}

		return numeros;
	}

	/**
	 * Imprime todos os números
	 * 
	 * @param numeros
	 * @param l
	 */

	public static void imprimirNumeros(String[] numeros, int l) {
		for (int i = 0; i < numeros.length; i++) {
			imprimeNumero(i, numeros, l);
		}
	}

	/**
	 * Imprime um número.
	 * 
	 * @param numero
	 * @param numerosCod
	 * @param l
	 */
	private static void imprimeNumero(int numero, String[] numerosCod, int l) {
		System.out.println("Número " + numero + ":");
		for (int i = 0; i < numerosCod[numero].length(); i = i + l) {
			System.out.println(numerosCod[numero].substring(i, i + l));
		}
	}

	/**
	 * Pega os número que serão utilizados na operação
	 * 
	 * @param linhasDoArquivo
	 * @param h
	 * @param l
	 * @param numLinhas
	 * @param linhaRef
	 * @return
	 */
	public static String[] getNumerosDaOperacao(List<String> linhasDoArquivo, int h, int l, int numLinhasDoNumero,
			int linhaRef) {
		String[] numeros = new String[numLinhasDoNumero / h];

		// zera os números
		for (int i = 0; i < numeros.length; i++) {
			numeros[i] = "";
		}

		for (int index = 0; index < numeros.length; index++) {
			for (int i = linhaRef; i < linhaRef + h; i++) {
				numeros[index] = numeros[index] + linhasDoArquivo.get(i).substring(0, l);
			}
			linhaRef = linhaRef + h;

		}

		return numeros;

	}

	/**
	 * Calcula o número na base 20
	 * 
	 * @param numero
	 * @param numerosRef
	 * @return
	 */
	public static BigDecimal calcularNumeroBase20(String[] numero, String[] numerosRef) {
		BigDecimal resultado = new BigDecimal("0.0");
		for (int i = 0; i < numero.length; i++) {
			int numeroReal = getNumero(numero[i], numerosRef);
			BigDecimal fatorial = new BigDecimal(Math.pow(20, numero.length - 1 - i) * numeroReal);
			resultado = resultado.add(fatorial);
		}
		return resultado;

	}

	/**
	 * Pega o número a partir da lista de referencia
	 * 
	 * @param numero
	 * @param numerosRef
	 * @return
	 */
	private static int getNumero(String numero, String[] numerosRef) {
		for (int i = 0; i < numerosRef.length; i++) {
			if (numero.equals(numerosRef[i])) {
				return i;
			}
		}
		// resultado falso (número não existe na base)
		return 99999;
	}

	/**
	 * Realiza a operação
	 * 
	 * @param s1
	 * @param s2
	 * @param operacao
	 * @return
	 */
	public static BigDecimal realizarOperacao(BigDecimal s1, BigDecimal s2, String operacao) {

		BigDecimal resultado = new BigDecimal(0.0);
		switch (operacao) {

		case "+": {
			resultado = s1.add(s2);
			break;
		}
		case "-": {
			resultado = s1.subtract(s2);
			break;
		}
		case "*": {
			resultado = s1.multiply(s2);
			break;
		}
		case "/": {
			resultado = s1.divide(s2);
			break;
		}
		}
		return resultado;
	}

	/**
	 * Converte valor decimal para Array de inteiros na base 20
	 * 
	 * @param resultado
	 * @return lista com os resutados na base 20
	 */
	public static List<Integer> converteDecimalParaBase20(BigDecimal valor) {
		int resto = -1;
		// definição para base 20
		BigDecimal base = new BigDecimal(20);

		List<Integer> resultado = new ArrayList<>();

		// enquanto o resto da divisão for maior ou igual a base
		while ((valor.compareTo(base) == 0) || (valor.compareTo(base) == 1)) {
			// quarda o valor inteiro do resto
			resto = valor.remainder(base).intValue();
			// calcula o próximo valor obtido pela a divisão pela a base.
			valor = valor.divide(base);
			// adiciona o resto a lista
			resultado.add(resto);
		}
		// ultimo resultado caso o valor senha menor ou igual a base
		resultado.add(valor.intValue());
		// inverte a lista
		Collections.reverse(resultado);

		return resultado;
	}

	/**
	 * Verifica se os valores dos número s1 e s2 excedem 2^63. 
	 * @param vrS1
	 * @param vrS2
	 * @return true se excede, false se não excede. 
	 */
	public static boolean s1ES2ExcedeValorMaximo(BigDecimal vrS1, BigDecimal vrS2) {

		// valor máximo 2^63
		BigDecimal vrMax = new BigDecimal(Math.pow(2, 63));
		// se vrS1 é maior que o valor máximo definido retorna true
		if ((vrS1.compareTo(vrMax) == 0) || (vrS1.compareTo(vrMax) == 1)) {
			return true;
		}
		// se vrS2 é maior que o valor máximo definido retorna true
		if ((vrS2.compareTo(vrMax) == 0) || (vrS2.compareTo(vrMax) == 1)) {
			return true;
		}
		return false;
	}

	/**
	 * Codifica o resultado gráfico a partir a entradas
	 * 
	 * @param resultadoBase20
	 * @param numerosRef
	 * @return resultado codigicado.
	 */
	public static String codBase20(List<Integer> resultadoBase20, String[] numerosRef) {

		String cod = "";
		for (int i = 0; i < resultadoBase20.size(); i++) {
			int num = resultadoBase20.get(i);
			cod = cod + numerosRef[num];
		}
		return cod;

	}

	/**
	 * Imprime o rsultado codificado na base 20
	 * 
	 * @param resultadoDeCod
	 * @param l
	 */
	public static void imprimirResultadoCod(String resultadoDeCod, int l) {
		System.out.println("Resultado codificado na base 20:");
		for (int i = 0; i < resultadoDeCod.length(); i = i + l) {
			System.out.println(resultadoDeCod.substring(i, i + l));
		}
	}

	/**
	 * Verifica se tem algum restrição
	 * 
	 * @return true caso exista alguma restrição e false caso não tenha.
	 */
	public static boolean temRestricaoLH(int l, int h) {
		// verficar se h pertence ao intervalo 0 < h < 100. Se não pertencer
		// retorna que tem restrição (true).
		if (!(0 < h && h < 100)) {
			return true;
		}
		// verficar se l pertence ao intervalo 0 < l < 100. Se não pertencer
		// retorna que tem restrição (true).
		else if (!(0 < l && l < 100)) {
			return true;
		} // verficar se valor de s1 pertence ao intervalo 0 < vrs1 < 100. Se
			// não pertencer
			// retorna que tem restrição (true).

		return false;
	}

	/**
	 * Verifica se tem algum restrição
	 * 
	 * @return true caso exista alguma restrição e false caso não tenha.
	 */
	public static boolean temRestricaoVrS1VrS2(double vrS1, double vrS2) {
		// verficar se valor de s1 pertence ao intervalo 0 < vrs1 < 100. Se
		// não pertencer
		// retorna que tem restrição (true).
		if (!(0 < vrS1 && vrS1 < 100)) {
			return true;
		}
		// verficar se valor de s2 pertence ao intervalo 0 < vrs2 < 100. Se não
		// pertencer
		// retorna que tem restrição (true).
		else if (!(0 < vrS2 && vrS2 < 100)) {
			return true;
		}
		return false;
	}

	/**
	 * Verifica se é uma operação válida
	 * 
	 * @param operacao
	 * @return
	 */
	public static boolean eOperacaoValida(String operacao) {

		switch (operacao.trim()) {

		case "+": {
			return true;
		}
		case "-": {
			return true;
		}
		case "*": {
			return true;
		}
		case "/": {
			return true;
		}
		}
		return false;
	}
	
	/**
	 * Pega o número com resultado na base 20
	 * @param resultadoB20
	 * @return
	 */
	public static String getResultadoBase20(List<Integer> resultadoB20){
		String resultado = "";
		for(Integer numero : resultadoB20){
			resultado = resultado + numero.intValue();
		}
		return resultado;
	}
}
