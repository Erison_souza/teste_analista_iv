package br.gov.teste.pratico.util;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import br.gov.teste.pratico.dao.UsuarioDAO;
import br.gov.teste.pratico.entity.Usuario;
import br.gov.teste.pratico.mb.LoginMB;

public class SessaoUtil {

	/**
	 * Retorna o Login da sessão do usuário que está logado
	 * 
	 * @return 
	 */
	private static String URL_LOGIN = "/login.xhtml?faces-redirect=true";

	public static LoginMB getLogin() {
		ExternalContext externalContext = FacesContext.getCurrentInstance()
				.getExternalContext();
		HttpSession session = (HttpSession) externalContext.getSession(true);
		LoginMB loginBean = (LoginMB) session
				.getAttribute("loginMB");
		
		return loginBean;
	}
	
	/**
	 * Pegar usuário
	 * @return
	 */
	
	public static Usuario getUsuario(){
		UsuarioDAO  usuarioDao;
		usuarioDao = new UsuarioDAO(Usuario.class);
		LoginMB loginBean = SessaoUtil.getLogin();
		return loginBean.getUsuario();
	}
	
	/**
	 * Verifica se está logado
	 */
	public static boolean isLogado(){
		if(getUsuario().isLogado()){
			return true;
		}
		return false;
	}
	
	public static String retornarParaPagLogin(){
		return URL_LOGIN;

	}
}
