package br.gov.teste.pratico.util;

import java.util.ResourceBundle;

import javax.faces.context.FacesContext;

public class MensagensUtil {

	/**
	 * Pega a mensagem do messages.properties
	 * @param mensagem
	 * @return
	 */
	public static String getMessagem(String mensagem){
		FacesContext context = FacesContext.getCurrentInstance();
		ResourceBundle.getBundle("/messages", context.getViewRoot().getLocale());
		ResourceBundle bundle = context.getApplication().getResourceBundle(context, "msg");
		return bundle.getString(mensagem);
	}
}
