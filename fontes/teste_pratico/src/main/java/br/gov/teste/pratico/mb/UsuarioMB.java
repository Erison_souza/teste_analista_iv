package br.gov.teste.pratico.mb;

import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import br.gov.teste.pratico.dao.UsuarioDAO;
import br.gov.teste.pratico.entity.Usuario;
import br.gov.teste.pratico.util.MD5Util;
import br.gov.teste.pratico.util.MensagensUtil;
import br.gov.teste.pratico.util.SessaoUtil;
import br.gov.teste.pratico.util.ValidarCpfUtil;

/**
 * Classe Bean de usuário onde serão contidos, valores e métodos para o mesmo
 * 
 * @author Erison
 *
 */
@ManagedBean
@SessionScoped
public class UsuarioMB implements InterfaceMB {

	/****************************
	 * URL´S DE TELA
	 ****************************/
	private static String URL_CADASTRO = "/pages/usuario/formulario.xhtml?faces-redirect=true";
	private static String URL_LISTA = "/pages/usuario/listar.xhtml?faces-redirect=true";
	private static String URL_VISUALIZAR = "/pages/usuario/visualizar.xhtml?faces-redirect=true";

	/*********************************
	 * DAO
	 *********************************/

	private UsuarioDAO usuarioDao;
	/***************************
	 * VARIÁVEIS
	 ******************************/

	private Usuario usuario;
	private Usuario usuarioLogado;
	private String confirmeSenha;
	private List<Usuario> usuarios = new ArrayList<Usuario>();
	private boolean paginaInserir;
	private boolean criptogravaSenha;
	private Usuario usuarioFiltro;

	/***
	 * CONSTRUTOR
	 */
	public UsuarioMB() {
		setPaginaInserir(true);
		setCriptogravaSenha(true);
		this.usuarioLogado = SessaoUtil.getUsuario();
		if (usuarioLogado.isLogado()) {
			usuarioFiltro = new Usuario();
			this.usuarioDao = new UsuarioDAO(Usuario.class);
			usuario = new Usuario();
			pesquisar();
		} else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					MensagensUtil.getMessagem("acesso_negado"), MensagensUtil.getMessagem("acesso_negado")));
		}
	}

	/**************************
	 * MÉTODOS
	 *************************/
	@Override
	public String cadastrar() {
		// TODO Auto-generated method stub
		usuario = new Usuario();
		setPaginaInserir(true);
		setCriptogravaSenha(true);
		return URL_CADASTRO;
	}

	@Override
	public String listar() {
		// TODO Auto-generated method stub
		limparPesquisa();
		return URL_LISTA;
	}

	@Override
	public String alterar() {
		// TODO Auto-generated method stub
		// seta a senha para confirmação com a mesma senha do usuário
		setConfirmeSenha(usuario.getSenha());
		setPaginaInserir(false);
		setCriptogravaSenha(false);
		return URL_CADASTRO;
	}

	/**
	 * Inativa o usuário. Esse método faz a exclusão lógica e usuários.
	 */
	@Override
	public String inativar() {
		// TODO Auto-generated method stub
		// não pode deixar desativar o usuário logado
		if (!(usuario.getIdUsuario() == SessaoUtil.getUsuario().getIdUsuario())) {
			// seta a flag para inativa o usuário
			this.usuario.setAtivo(false);
			// atualiza o usuário
			usuarioDao.atualiza(usuario);
			// refaz a pesquisa de usuários
			pesquisar();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
					"Usuário " + usuario.getNome() + " inativado.", "Usuário " + usuario.getNome() + "inativo."));
		} else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					MensagensUtil.getMessagem("MN006"), MensagensUtil.getMessagem("MN006")));
		}

		return null;
	}

	/**
	 * Salva um usuário cadastrando um novo ou alterando um antigo.
	 */
	@Override
	public String salvar() {
		// TODO Auto-generated method stub
		if (!usuario.getLogin().equals("admin")) {
			//validar se é um cpf válido
			if (ValidarCpfUtil.eCpfValido(usuario.getCpf())) {
				// verifica se o cpf já não foi cadastrado para outro usuário
				if (!usuarioDao.cpfJaCadastrado(usuario)) {
					if (!usuarioDao.loginJaCadastrado(usuario)) {
						// verifica se a senha escolhida é igual a senha
						// confirmada
						if (usuario.getSenha().equals(confirmeSenha)) {
							try {
								// seta o usuário para ter o status ativo
								usuario.setAtivo(true);
								// criptograva a senha do usuário em MD5 caso
								// seja cadastro
								if (isPaginaInserir() || criptogravaSenha) {
									usuario.setSenha(MD5Util.criptografarMD5(usuario.getSenha()));
								}
								// salva o uusário
								usuarioDao.salvar(usuario, usuario.getIdUsuario());
								// refaz a pesquisa
								pesquisar();
								return URL_LISTA;
							} catch (Exception e) {
								FacesContext.getCurrentInstance().addMessage(null,
										new FacesMessage(FacesMessage.SEVERITY_ERROR,
												MensagensUtil.getMessagem("MN003"),
												MensagensUtil.getMessagem("MN003")));
								e.printStackTrace();
								return null;
							}
						} else {
							FacesContext.getCurrentInstance().addMessage(null,
									new FacesMessage(FacesMessage.SEVERITY_ERROR, MensagensUtil.getMessagem("MN002"),
											MensagensUtil.getMessagem("MN002")));
							return null;
						}
					} else {
						FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
								MensagensUtil.getMessagem("MN001"), MensagensUtil.getMessagem("MN001")));
						return null;
					}
				} else {
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
							MensagensUtil.getMessagem("MN004"), MensagensUtil.getMessagem("MN004")));
					return null;

				}
			} else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
						MensagensUtil.getMessagem("MN018"), MensagensUtil.getMessagem("MN018")));
				return null;
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					MensagensUtil.getMessagem("MN005"), MensagensUtil.getMessagem("MN005")));
			return null;
		}
	}

	/**
	 * verificar se já existe cadastro de login para esse usuário e apresenta a
	 * mensagem
	 */
	public void verificarSeloginEsteParaOutroUsuario() {
		if (usuarioDao.loginJaCadastrado(usuario)) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					MensagensUtil.getMessagem("MN001"), MensagensUtil.getMessagem("MN001")));
		}
	}

	/**
	 * Lista e pesquisar por usuários
	 */
	@Override
	public String pesquisar() {
		// TODO Auto-generated method stub
		setUsuarios(usuarioDao.listar(usuarioFiltro));
		return null;
	}

	/**
	 * Altera a senha do usuário
	 */
	public void alterarSenhaDoUsuario() {
		setCriptogravaSenha(true);
		if (usuario.getSenha().equals(confirmeSenha)) {
			try {
				// criptograva a senha do usuário em MD5
				usuario.setSenha(MD5Util.criptografarMD5(usuario.getSenha()));
				// salva o uusário
				//usuarioDao.salvar(usuario, usuario.getIdUsuario());
				usuario.setSenha(confirmeSenha);
				RequestContext.getCurrentInstance().execute("PF('alterarSenhaDialog').hide()");
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
						MensagensUtil.getMessagem("MN007"), MensagensUtil.getMessagem("MN007")));
			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
						MensagensUtil.getMessagem("MN003"), MensagensUtil.getMessagem("MN003")));
				e.printStackTrace();
			}
		} else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					MensagensUtil.getMessagem("MN002"), MensagensUtil.getMessagem("MN002")));
		}
	}

	/**
	 * Limpa o filtro de pesquisa de usário
	 */
	public void limparPesquisa() {
		usuarioFiltro = new Usuario();
		pesquisar();
	}

	@Override
	public String visualizar() {
		// TODO Auto-generated method stub
		return URL_VISUALIZAR;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}

	public void setUsuarioLogado(Usuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}

	public String getConfirmeSenha() {
		return confirmeSenha;
	}

	public void setConfirmeSenha(String confirmeSenha) {
		this.confirmeSenha = confirmeSenha;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public boolean isPaginaInserir() {
		return paginaInserir;
	}

	public void setPaginaInserir(boolean paginaInserir) {
		this.paginaInserir = paginaInserir;
	}

	public Usuario getUsuarioFiltro() {
		return usuarioFiltro;
	}

	public void setUsuarioFiltro(Usuario usuarioFiltro) {
		this.usuarioFiltro = usuarioFiltro;
	}

	public boolean isCriptogravaSenha() {
		return criptogravaSenha;
	}

	public void setCriptogravaSenha(boolean criptogravaSenha) {
		this.criptogravaSenha = criptogravaSenha;
	}

}
