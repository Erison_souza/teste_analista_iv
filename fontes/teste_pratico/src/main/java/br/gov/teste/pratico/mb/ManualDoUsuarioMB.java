package br.gov.teste.pratico.mb;

import java.io.InputStream;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

@SessionScoped
@ManagedBean
public class ManualDoUsuarioMB {

	private StreamedContent manual;

	public ManualDoUsuarioMB() {

	}

	public StreamedContent getManual() {
		return manual;
	}

	/**
	 * Faz download do manuãl do usuário
	 */
	public void downloadManual() {
		InputStream stream = ((ServletContext) FacesContext
				.getCurrentInstance().getExternalContext().getContext())
				.getResourceAsStream("/manuais/Manual_Do_Usuario.pdf");
		manual = new DefaultStreamedContent(stream, "application/pdf",
				"Manual_do_Usuario.pdf");
		
		RequestContext.getCurrentInstance().execute(
				"document.getElementById('form_menu:manual_usuario').click();");
	}

	public void setManual(StreamedContent manual) {
		this.manual = manual;
	}

}
