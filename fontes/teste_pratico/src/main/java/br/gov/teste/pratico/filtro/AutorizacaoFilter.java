package br.gov.teste.pratico.filtro;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.gov.teste.pratico.mb.LoginMB;


@WebFilter(urlPatterns = { "/pages/*", "/index.xhtml", "/acessoNegado.xhtml" })
public class AutorizacaoFilter implements Filter {

	private static String URL_LOGIN = "/login.xhtml?faces-redirect=true";

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		String contextPath = ((HttpServletRequest) request).getContextPath();

		if (((HttpServletRequest) request).getSession().getAttribute("loginMB") == null) {
			((HttpServletResponse) response).sendRedirect(contextPath + "/login.xhtml");
			
		} else {
			LoginMB login = (LoginMB) ((HttpServletRequest) request).getSession()
					.getAttribute("loginMB");
			// Se os campos Forem Brancos //Login for errado
			try {
				if (login.getUsuario().isLogado()) {
					// Permissao para Continuar Filtro
					chain.doFilter(request, response);
				} else {
					// Redirect para Login
					((HttpServletResponse) response).sendRedirect(contextPath + "/login.xhtml");

				}
			} catch (NullPointerException npe) {
				// npe.printStackTrace();

				((HttpServletResponse) response).sendRedirect(contextPath + "/login.xhtml");
			}

		}
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
	}

	@Override
	public void destroy() {
	}
/*
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		try {
			// chain...
			HttpServletRequest request_ = (HttpServletRequest) request;

			if (isAjax(request_) && !request_.isRequestedSessionIdValid()) {
				//log.warn("Session expiration during ajax request, partial redirect to login page");
				HttpServletResponse response_ = (HttpServletResponse) response;
				response_.getWriter().print(xmlPartialRedirectToPage(request_, "/login?session_expired=1"));
				response_.flushBuffer();
			} else {
				chain.doFilter(request, response);
			}

		} catch (Exception e) {
			// redirect to error page
			HttpServletRequest request_ = (HttpServletRequest) request;
			request_.getSession().setAttribute("lastException", e);
			request_.getSession().setAttribute("lastExceptionUniqueId", e.hashCode());

			//log.error("EXCEPTION unique id: " + e.hashCode(), e);

			HttpServletResponse response_ = (HttpServletResponse) response;

			if (!isAjax(request_)) {
				response_.sendRedirect(request_.getContextPath() + request_.getServletPath() + "/error");
			} else {
				// let's leverage jsf2 partial response
				response_.getWriter().print(xmlPartialRedirectToPage(request_, "/error"));
				response_.flushBuffer();
			}
		}
	}
	*/

	private String xmlPartialRedirectToPage(HttpServletRequest request, String page) {
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version='1.0' encoding='UTF-8'?>");
		sb.append("<partial-response><redirect url=\"").append(request.getContextPath())
				.append(request.getServletPath()).append(page).append("\"/></partial-response>");
		return sb.toString();
	}

	private boolean isAjax(HttpServletRequest request) {
		return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
	}
	
}
