package br.gov.teste.pratico.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import br.gov.teste.pratico.conection.ConectionJPA;
import br.gov.teste.pratico.entity.Usuario;
import br.gov.teste.pratico.util.MD5Util;

public class UsuarioDAO extends DAO<Usuario> {

	public UsuarioDAO(Class<Usuario> classe) {
		super(classe);
		entityManager = new ConectionJPA().getEntityManager();
		// TODO Auto-generated constructor stub
	}

	private EntityManager entityManager;

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/***
	 * Faz login do sistema credenciais.
	 * 
	 * @param login
	 * @param senha
	 * @return usuário logado ou null se não encontra nenhum usuário com as
	 *         credenciais informadas.
	 */
	public Usuario fazerLogin(String login, String senha) {

		entityManager = new ConectionJPA().getEntityManager();
		try {
			senha = MD5Util.criptografarMD5(senha);

			final StringBuilder sbQuery = new StringBuilder();

			sbQuery.append(" SELECT u ");
			sbQuery.append(" FROM Usuario u ");
			sbQuery.append(" where u.login = :paramLogin  ");
			sbQuery.append(" and u.senha = :paramSenha ");
			sbQuery.append(" and u.ativo = true ");

			Query query = entityManager.createQuery(sbQuery.toString());

			query.setParameter("paramLogin", login);
			query.setParameter("paramSenha", senha);
			try {
				Usuario usuario = (Usuario) query.getSingleResult();
				entityManager.close();
				return usuario;
			} catch (NoResultException e) {
				return null;
			}

		} catch (Exception e1) {

			e1.printStackTrace();
		}
		entityManager.close();
		return null;

	}

	/**
	 * Procurar usuário pelo seu ID
	 * 
	 * @author Erison
	 * @param idUsuario
	 * @return o usuário que está procurando ou null caso não enconte nenhum
	 *         usuário.
	 */
	public Usuario procurarPeloId(Integer idUsuario) {

		entityManager = new ConectionJPA().getEntityManager();

		final StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" SELECT u ");
		sbQuery.append(" FROM Usuario u ");
		sbQuery.append(" where u.idUsuario = :paramIdUsuario  ");
		sbQuery.append(" and u.ativo = true ");

		Query query = entityManager.createQuery(sbQuery.toString());

		query.setParameter("paramIdUsuario", idUsuario);
		try {
			Usuario usuario = (Usuario) query.getSingleResult();

			entityManager.close();
			return usuario;
		} catch (NoResultException e) {
			entityManager.close();
			return null;
		}

	}

	/**
	 * Verifica se o cpf já está cadastrdo para outro usuário
	 * 
	 * @param usuario
	 * @param inserir
	 * @return true caso já haja cadastro de cpf para outro usuário, false causo
	 *         não exista cadastro de cpf para outro usuário
	 */
	public boolean cpfJaCadastrado(Usuario usuario) {

		entityManager = new ConectionJPA().getEntityManager();

		final StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" SELECT u ");
		sbQuery.append(" FROM Usuario u ");
		sbQuery.append(" where u.cpf = :paramCpf  ");
		sbQuery.append(" and u.idUsuario <> :paramIdUsuario ");

		Query query = entityManager.createQuery(sbQuery.toString());

		query.setParameter("paramCpf", usuario.getCpf());
		query.setParameter("paramIdUsuario", usuario.getIdUsuario());

		List<Usuario> usuarios = query.getResultList();

		entityManager.close();
		if (usuarios.size() > 0) {
			return true;
		}
		return false;
	}

	/**
	 * Verifica se o login já está cadastrdo para outro usuário
	 * 
	 * @param usuario
	 * @param inserir
	 * @return true caso já haja cadastro de login para outro usuário, false
	 *         causo não exista cadastro de login para outro usuário
	 */
	public boolean loginJaCadastrado(Usuario usuario) {

		entityManager = new ConectionJPA().getEntityManager();

		final StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" SELECT u ");
		sbQuery.append(" FROM Usuario u ");
		sbQuery.append(" where u.login = :paramLogin  ");
		sbQuery.append(" and u.idUsuario <> :paramIdUsuario ");

		Query query = entityManager.createQuery(sbQuery.toString());

		query.setParameter("paramLogin", usuario.getLogin());
		query.setParameter("paramIdUsuario", usuario.getIdUsuario());

		List<Usuario> usuarios = query.getResultList();

		if (usuarios.size() > 0) {
			return true;
		}
		return false;
	}

	/**
	 * Lista usuários através do filtro. Caso o filtro seja nulo ou seus valores
	 * sejam nulos, lista todos os usuários ativos.
	 * 
	 * @param usuarioFiltro
	 * @return lista de usuários
	 */
	public List<Usuario> listar(Usuario usuarioFiltro) {

		entityManager = new ConectionJPA().getEntityManager();
		final StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" SELECT usu ");
		sbQuery.append(" FROM Usuario usu ");
		sbQuery.append(" where usu.ativo = true ");

		// filtro por nome do usuário

		if (usuarioFiltro != null) {
			if ((usuarioFiltro.getNome() != null) && !usuarioFiltro.getNome().trim().equals("")) {
				sbQuery.append(" AND usu.nome like :nome ");
			}

			if ((usuarioFiltro.getLogin() != null) && !usuarioFiltro.getLogin().trim().equals("")) {
				sbQuery.append(" AND usu.login like :login ");
			}

			if ((usuarioFiltro.getCpf() != null) && !usuarioFiltro.getCpf().trim().equals("")) {
				sbQuery.append(" AND usu.cpf =:cpf ");
			}
		}

		final Query query = entityManager.createQuery(sbQuery.toString());

		if (usuarioFiltro != null) {
			if ((usuarioFiltro.getNome() != null) && !usuarioFiltro.getNome().trim().equals("")) {

				query.setParameter("nome", "%" + usuarioFiltro.getNome() + "%");
			}

			if ((usuarioFiltro.getLogin() != null) && !usuarioFiltro.getLogin().trim().equals("")) {
				query.setParameter("login", "%" + usuarioFiltro.getLogin() + "%");
			}
			if ((usuarioFiltro.getCpf() != null) && !usuarioFiltro.getCpf().trim().equals("")) {

				query.setParameter("cpf", usuarioFiltro.getCpf());
			}

		}

		List<Usuario> lista = query.getResultList();
		entityManager.close();

		return lista;

	}

}
